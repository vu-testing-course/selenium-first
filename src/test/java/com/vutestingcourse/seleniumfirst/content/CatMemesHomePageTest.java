package com.vutestingcourse.seleniumfirst.content;

import com.vutestingcourse.seleniumfirst.catMemesPage.CatMemesHomePage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.time.Duration;

public class CatMemesHomePageTest {
  public static WebDriver driver;
  public static CatMemesHomePage homePage;

  @BeforeAll
  public static void setup() {
    driver = new ChromeDriver();
    homePage = new CatMemesHomePage(driver);
    driver.get(homePage.getUrl());

    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    driver.manage().window().maximize();
  }

  @AfterAll
  public static void tearDown() {
    driver.close();
    driver.quit();
  }

  @Test
  public void verifyMotoText() {
    String expected = "If there's one thing that the internet was made for, it's funny cat memes.";
    String actual = homePage.getMotoText();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void verifyHeaderText() {
    String expected = "Cat memes";
    String actual = homePage.getHeaderText();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void verifyLogoText() {
    String expected = "Cats album";
    String actual = homePage.getLogoText();
    Assertions.assertEquals(expected, actual);
  }
}
