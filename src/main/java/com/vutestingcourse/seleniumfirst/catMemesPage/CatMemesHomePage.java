package com.vutestingcourse.seleniumfirst.catMemesPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

@Component
public class CatMemesHomePage {
  private static final String url = "https://suninjuly.github.io/cats.html";

  private final WebDriver driver;
  private final By header = By.cssSelector("html body main section.jumbotron.text-center div.container h1.jumbotron-heading");
  private final By logo = By.className("navbar-brand");
  private final By moto = By.xpath("//*[@id=\"moto\"]");

  public CatMemesHomePage(WebDriver driver) {
    this.driver = driver;
  }

  public String getUrl() {
    return url;
  }

  public String getHeaderText() {
    return getText(header);
  }

  public String getMotoText() {
    return getText(moto);
  }

  public String getLogoText() {
    return getText(logo);
  }

  private String getText(By from) {
    return driver.findElement(from).getText();
  }
}
