package com.vutestingcourse.seleniumfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumFirstApplication {
  public static void main(String[] args) {
    SpringApplication.run(SeleniumFirstApplication.class, args);
  }
}
